// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var tabsBox = $(".tabs"),
	modal = $("[data-modal]"),
	equal = $(".maxheight"),
	tabsBox = $(".tabs"),
	gerbCarousel = $(".gerb_carousel"),
	stepCarousel = $(".step_carousel"),
	dashboardSlider = $(".dashboard2_box_slider_item"),
	professionSlider = $(".professions_slider_box"),
	dash2 = $('.dash2'),
	otherMissionsSlider = $('.other_missions_slider'),
	tapeCarousel = $('.tape_carousel'),
	gerbCarousel2 = $(".gerb_carousel_2"),
	styler = $(".styler"),
	scrollContainer = $(".scroll_container"),
	scrollMouse = $(".scroll_mouse"),
	msgField = $("#msg_field"),
	gallery = $(".gallery"),
	stripeBox = $(".stripe_box"),
	slider3d = $('.slider3d'),
	sticky = $(".sticky"),
	tagsSelect = $('.search_tags'),
	materialCarousel = $('.material_carousel'),
	sticker = $('#sticker');

if(equal.length){
  include("js/jquery.equalheights.js");
}
if(tagsSelect.length){
  include("js/select2.full.js");
}
if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}
if(modal.length){
  include("js/jquery.arcticmodal.js");
}
if(gerbCarousel.length || dashboardSlider.length || dash2.length || otherMissionsSlider.length || tapeCarousel.length || materialCarousel.length || stepCarousel.length || professionSlider.length){
  include("js/owl.carousel.js");	
}
if(styler.length){
  include("js/formstyler/formstyler.js");	
}
if(gallery.length){
  include("js/jquery.gallery.js");	
  include("js/modernizr.custom.53451.js");	
}
if(scrollContainer.length){
	include("js/jquery.jscrollpane.js");		
}
if(scrollMouse.length){
	include("js/jquery.mousewheel.js");		
}
if(slider3d.length){
	include("js/jquery.waterwheelCarousel.js");			
}
if(sticky.length || sticker.length){
	include("js/jquery.sticky.js");
	include("js/jquery.smoothscroll.js");
}

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(window).load(function(){

	$(window).bind('mousewheel', function(event) {
	    if (event.originalEvent.wheelDelta >= 0) {
	      $('.sticky-wrapper').addClass('wind_up');
	    } else {
	      $('.sticky-wrapper').removeClass('wind_up');
	    }
	});

  // ДЕЛАЕМ ИНИТ ФУНКЦИЙ ЗДЕСЬ


 	$(".search_agents").on( "keyup", '.select2-search__field', function(){
		$(this).closest('.search_list').addClass("active");
		$(this).closest('.search_list_item').addClass("active");
	});



	



	$(".accordion_list li .accordion_list_title").on( "click", function(){	
		$(this).parent().toggleClass('active');
	});

  	function stripeBoxFunc(){
  		if (stripeBox.length){

		  	stripeBox.each(function(){
		  		var current = $(this),
		  			currentW 	= current.width(),
		  			currentFill = current.find(".stripe_box_fill"),
		  			currentUnfill = current.find('.stripe_box_unfill'),
		  			fillWidth 	= parseInt(currentFill.data("val")),
		  			appendElem 	= '<span class="stripe_box_val">'+fillWidth+'%</span>',
		  			totalFillW  = currentW / 100 * fillWidth;

		  		currentFill.css({width: totalFillW});

		  		currentFill.append(appendElem);

		  		currentUnfill.css({width: (currentW - totalFillW - 7) - current.find(".stripe_box_val").width()});

		  	})

  		}
  	}
  	// stripeBoxFunc();

  	if ($("[data-qty-code]").length){
  		var codeBtn = $("[data-qty-code]"),
  			codeBtnVal = codeBtn.data('qty-code');

  		for (var i = 1; i <= codeBtnVal; i++) {
  			codeBtn.append('<i></i>')
  		}
  	}

  if($(".teather_1_3").length){

  	var counter = 0;

  	$(".teather_1_3").each(function(){
  		var currentItem = $(this),
  			fillColor   = currentItem.data('fill'),
  			currentImg  = currentItem.find(">.teather_img_wrap img"),
  			imageData   = {
  				width  : currentImg.width(),
  				height : currentImg.height(),
  				url    : currentImg.attr("src")
  			},
  			svg1 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 388,0 388,248 0,248 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 388 0 L 388 246 L 0 246 L 0 30 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg2 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,217 357,248 0,248 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0 0 L 388 0 L 388 217 L 357 248 L 0 248 L 0 32 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg3 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 388,0 388,217 357,248 0,248 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 388 0 L 388 217 L 357 248 L 0 248 L 0 32 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg4 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,217 357,248 0,248 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0 0 L 388 0 L 388 217 L 357 248 L 0 248 L 0 32 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg5 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 388,0 388,300 0,300 0,32" fill="#065b94" fill-opacity=".2" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 388 0 L 388 300 L 0 300 L 0 30 z" stroke="none" fill-opacity="" fill="url(#image_pattern'+counter+')" />',
			svg6 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,300 357,300 0,300 0,0" fill="#065b94" fill-opacity=".2" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0,0 L 388,0 388,300 357,300 0,300 0,0 z" stroke="none" fill-opacity="" fill="url(#image_pattern'+counter+')" />',
			svg7 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 356,0 388,32 388,300 0,300 0,0" fill="#065b94" fill-opacity=".2" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0,0 L 356,0 388,32 388,300 0,300 0,0 z" stroke="none" fill-opacity="" fill="url(#image_pattern'+counter+')" />',
			svg8 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,300 32,300 0,268 0,0" fill="#065b94" fill-opacity=".2" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0,0 L 388,0 388,300 32,300 0,268 0,0 z" stroke="none" fill-opacity="" fill="url(#image_pattern'+counter+')" />',
			svg9 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,268 357,300 0,300 0,0" fill="#065b94" fill-opacity=".2" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0,0 L 388,0 388,268 357,300 0,300 0,0 z" stroke="none" fill-opacity="" fill="url(#image_pattern'+counter+')" />',
			svg10 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,251 0,251 0,0" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0,0 L 388,0 388,251 0,251 0,0 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />';

		if (fillColor){
			svg1 += '<path class="fill_color" d="M 32 0 L 388 0 L 388 246 L 0 246 L 0 30 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg2 += '<path class="fill_color" d="M 0 0 L 388 0 L 388 217 L 357 248 L 0 248 L 0 32 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg3 += '<path class="fill_color" d="M 32 0 L 388 0 L 388 217 L 357 248 L 0 248 L 0 32 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg4 += '<path class="fill_color" d="M 0 0 L 388 0 L 388 217 L 357 248 L 0 248 L 0 32 z"  fill-opacity="0" fill="'+fillColor+'" />';
			svg5 += '<path class="fill_color" d="M 32 0 L 388 0 L 388 246 L 0 246 L 0 30 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg6 += '<path class="fill_color" d="M 0 0 L 388 0 L 388 248 L 357 248 L 0 248 L 0 0 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg7 += '<path class="fill_color" d="M 0,0 L 356,0 388,32 388,248 0,248 0,0 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg8 += '<path class="fill_color" d="M 0,0 L 388,0 388,248 32,248 0,216 0,0 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg9 += '<path class="fill_color" d="M 0,0 L 388,0 388,268 357,300 0,300 0,0 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
			svg10 += '<path class="fill_color" d="M 0,0 L 388,0 388,251 0,251 0,0 z" stroke="none" fill-opacity="0" fill="'+fillColor+'" />';
		}

		if(currentItem.hasClass("var1")){
			currentImg.hide().parent().append(svg1 + '</svg>');
		}
		else if(currentItem.hasClass("var2")){
  			currentImg.hide().parent().append(svg2 + '</svg>');
		}
		else if(currentItem.hasClass("var3")){
			currentImg.hide().parent().append(svg3 + '</svg>');
		}
		else if(currentItem.hasClass("var4")){
			currentImg.hide().parent().append(svg4 + '</svg>');
		}
		else if(currentItem.hasClass("var5")){
			currentImg.hide().parent().append(svg5 + '</svg>');
		}
		else if(currentItem.hasClass("var6")){
			currentImg.hide().parent().append(svg6 + '</svg>');
		}
		else if(currentItem.hasClass("var7")){
			currentImg.hide().parent().append(svg7 + '</svg>');
		}
		else if(currentItem.hasClass("var8")){
			currentImg.hide().parent().append(svg8 + '</svg>');
		}
		else if(currentItem.hasClass("var9")){
			currentImg.hide().parent().append(svg9 + '</svg>');
		}
		else if(currentItem.hasClass("var10")){
			currentImg.hide().parent().append(svg10 + '</svg>');
		}

		counter++;




  	})
  }

  if($(".new_svg_mission_container").length){
  	var counter = 0;

  	$(".new_svg_mission_container").each(function(){
  		var currentItem = $(this),
  			parentBoxes = $(".new_svg_mission_container"),
  			fillColor   = currentItem.data('fill'),
  			currentImg  = currentItem.find(">.new_svg_miss_box > img"),
  			imageData   = {
  				width  : currentImg.width(),
  				height : currentImg.height(),
  				url    : currentImg.attr("src")
  			},
  			parentSetting   = {
  				width  : parentBoxes.width(),
  				height : parentBoxes.height()
  			},
  			svg1 		= 	'<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
				          	'<pattern id="new_svg_miss'+counter+'" patternContentUnits="objectBoundingBox" width="100%" height="100%">' +
				            '<image xlink:href="'+imageData['url']+'" preserveAspectRatio="none" x="0" y="0" width="1" height="1"></image>' +
				          	'</pattern>' +
				          	'<polygon points="0,350 0,37 37,0 1200,0 1200,315 1165,350 " fill="none" stroke="#696d6f" stroke-width="2" /></polygon>' +
				          	'<path d="M 37,0 L 1199,1 1199,316 1168,350 1,350 1,37 z" stroke="none" fill="url(#new_svg_miss'+counter+')" />';

		if (fillColor){
			svg1 += '<path class="fill_color" d="M 37,0 L 1199,1 1199,316 1168,350 1,350 1,37 z" stroke="none"  fill="'+fillColor+'" />';
		}

		if(currentItem){
			currentImg.hide().parent().append(svg1 + '</svg>');
		}

		counter++;
  	})
  }

  if($(".new_svg_mission_container2").length){
  	var counter = 0;

  	$(".new_svg_mission_container2").each(function(){
  		var currentItem = $(this),
  			parentBoxes = $(".new_svg_mission_container2"),
  			fillColor   = currentItem.data('fill'),
  			currentImg  = currentItem.find(">.new_svg_miss_box2 > img"),
  			imageData   = {
  				width  : currentImg.width(),
  				height : currentImg.height(),
  				url    : currentImg.attr("src")
  			},
  			parentSetting   = {
  				width  : parentBoxes.width(),
  				height : parentBoxes.height()
  			},
  			svg1 		= 	'<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
				          	'<pattern id="new_svg_miss_2_'+counter+'" patternContentUnits="objectBoundingBox" width="100%" height="100%">' +
				            '<image xlink:href="'+imageData['url']+'" preserveAspectRatio="none" x="0" y="0" width="1" height="1"></image>' +
				          	'</pattern>' +
				          	'<polygon points="0,398 0,41 37,0 1200,0 1200,355 1165,398 " fill="none" stroke="#646668" stroke-width="2" /></polygon>' +
				          	'<path d="M 37,0 L 1199,1 1199,316 1168,398 1,398 1,37 z" stroke="none" fill-opacity="0.4" fill="url(#new_svg_miss_2_'+counter+')" />';

		if (fillColor){
			svg1 += '<path class="fill_color" d="M 37,0 L 1199,1 1199,316 1168,398 1,398 1,37 z" stroke="none"  fill="'+fillColor+'" />';
		}

		if(currentItem){
			currentImg.hide().parent().append(svg1 + '</svg>');
		}

		counter++;
  	})
  }

  if($(".dash2").length){
  	var counter = 0;

  	setTimeout(function(){

	  	$(".dash2").each(function(){
	  		var currentItem = $(this),
	  			parentBoxes = $(".dash2"),
	  			fillColor   = currentItem.data('fill'),
	  			currentImg  = currentItem.find(".my_mission_box_img_item > img"),
	  			imageData   = {
	  				width  : currentImg.width(),
	  				height : currentImg.height(),
	  				url    : currentImg.attr("src")
	  			},
	  			parentSetting   = {
	  				width  : parentBoxes.outerWidth(),
	  				height : parentBoxes.outerHeight()
	  			},
	  			positonImg   ={
	  				x  : parentBoxes.outerWidth() - currentImg.width() - 1
	  			},
	  			svg1 		= 	'<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
					          	'<pattern id="dash_svg_miss_3_'+counter+'" width="100%" height="100%">' +
					            '<image xlink:href="'+imageData['url']+'" x="'+positonImg['x']+'" y="0" width="'+imageData['width']+'" height="'+parentSetting['height']+'"></image>' +
					          	'</pattern>' +
					          	'<path d="M 0,0 L 1165,1 1199,34 1199,348 34,348 1,309 z" stroke="none" fill-opacity="0.5" fill="url(#dash_svg_miss_3_'+counter+')" />';

			if (fillColor){
				svg1 += '<path class="fill_color" d="M 0,0 L 1165,1 1199,34 1199,348 34,348 1,309 z" stroke="none"  fill="'+fillColor+'" />';
			}

			if(currentItem){
				currentImg.hide().parent().append(svg1 + '</svg>');
			}

			counter++;
	  	})

  	},300);
  }

  if($(".prof_svb_box").length){
  	var counter = 0;

  	$(".prof_svb_box").each(function(){
  		var currentItem = $(this),
  			parentBoxes = $(".prof_svb_box"),
  			fillColor   = currentItem.data('fill'),
  			currentImg  = currentItem.find(">.profile_desc_img > img"),
  			imageData   = {
  				width  : currentImg.outerWidth(),
  				height : currentImg.outerHeight(),
  				url    : currentImg.attr("src")
  			},
  			parentSetting   = {
  				width  : parentBoxes.outerWidth(),
  				height : parentBoxes.outerHeight()
  			},
  			svg1 		= 	'<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
				          	'<pattern id="prof_svb_box'+counter+'" patternContentUnits="objectBoundingBox" width="100%" height="100%">' +
				            '<image xlink:href="'+imageData['url']+'" preserveAspectRatio="none" x="0" y="0" width="1" height="1"></image>' +
				          	'</pattern>' +
				          	'<polygon points="' +generateCoords(parentSetting['width'], parentSetting['height']).join(' ')+ '" fill="#000000" fill-opacity=".5" stroke="#fff" stroke-width="1" /></polygon>' +
				          	'<path d="M 500,1 L 699,1 699,200 500,200 z" stroke="none" fill-opacity=".3" fill="url(#prof_svb_box'+counter+')" />';//M 0,0 L 700,0 700,558 667,600 0,600 z

		if (fillColor){
			svg1 += '<path class="fill_color" d="M 500,0 L 700,0 700,200 500,200 z" stroke="none" fill-opacity=".3" fill="'+fillColor+'" />';
		}

		if(currentItem){
			currentImg.hide().parent().append(svg1 + '</svg>');
		}

		counter++;
  	})
  }

  if($(".prof_svb_box2").length){
  	var counter = 0;

  	$(".prof_svb_box2").each(function(){
  		var currentItem = $(this),
  			parentBoxes = $(".prof_svb_box2"),
  			fillColor   = currentItem.data('fill'),
  			currentImg  = currentItem.find(">.profile_desc_img > img"),
  			imageData   = {
  				width  : currentImg.outerWidth(),
  				height : currentImg.outerHeight(),
  				url    : currentImg.attr("src")
  			},
  			parentSetting   = {
  				width  : parentBoxes.outerWidth(),
  				height : parentBoxes.outerHeight()
  			},
  			svg1 		= 	'<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
				          	'<pattern id="prof_svb_box2'+counter+'" patternContentUnits="objectBoundingBox" width="100%" height="100%">' +
				            '<image xlink:href="'+imageData['url']+'" preserveAspectRatio="none" x="0" y="0" width="1" height="1"></image>' +
				          	'</pattern>' +
				          	'<polygon points="' +generateCoords(parentSetting['width'], parentSetting['height']).join(' ')+ '" fill="#000000" fill-opacity="1" stroke="#fff" stroke-width="1" /></polygon>' +
				          	'<path d="M 550,1 L 699,1 699,150 550,150 z" stroke="none" fill-opacity="1" fill="url(#prof_svb_box2'+counter+')" />';//M 0,0 L 700,0 700,558 667,600 0,600 z

		if (fillColor){
			svg1 += '<path class="fill_color" d="M 500,0 L 700,0 700,150 500,150 z" stroke="none" fill-opacity=".3" fill="'+fillColor+'" />';
		}

		if(currentItem){
			currentImg.hide().parent().append(svg1 + '</svg>');
		}

		counter++;
  	})
  }

  if($(".current_mission").length){
  	var missionImg = $(".current_mission_img img"),
  		imageData = {
  			width: missionImg.width(),
  			height: missionImg.height(),
  			url: missionImg.attr("src"),
  		},
  		svgCurrentMission = '<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="sm_image_pattern'+counter1+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 650,0 650,117 610,148 0,148 0,32" fill="none" stroke="#fff" stroke-width="2" />' +
				          	'<path d="M 32 1 L 649 1 L 649 117 L 610 147 L 1 147 L 1 32 z" stroke="none" fill-opacity=".3" fill="url(#sm_image_pattern'+counter1+')" />'+
				          	'<path d="M 32 1 L 649 1 L 649 117 L 610 147 L 1 147 L 1 32 z" stroke="none" fill-opacity=".30" fill="#08a5e7" />'+
				        	'</svg>';

  	$(".current_mission").find(".current_mission_img").hide().parent().append(svgCurrentMission);
  }

  if($(".sm_skew_box").length){

  	var counter1 = 0;

  	$(".sm_skew_box").each(function(){
  		var currentItem = $(this),
  			currentImg  = currentItem.find("img"),
  			imageData   = {
  				width  : currentImg.width(),
  				height : currentImg.height(),
  				url    : currentImg.attr("src")
  			},
			svg1 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="sm_image_pattern'+counter1+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 389,0 389,119 357,149 0,149 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 1 L 389 1 L 389 119 L 357 149 L 0 149 L 0 32 z" stroke="none" fill-opacity=".8" fill="url(#sm_image_pattern'+counter1+')" />'+
				        	'</svg>',
			svg2 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="sm_image_pattern'+counter1+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 389,0 389,149 0,149 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 389 0 L 389 149 L 0 149 L 0 32 z" stroke="none" fill-opacity=".8" fill="url(#sm_image_pattern'+counter1+')" />'+
				        	'</svg>',
			svg3 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="sm_image_pattern'+counter1+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 389,0 389,117 357,149 0,149 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0 0 L 389 0 L 389 117 L 357 149 L 0 149 L 0 32 z" stroke="none" fill-opacity=".8" fill="url(#sm_image_pattern'+counter1+')" />'+
				        	'</svg>';

		if(currentItem.hasClass("var1")){
  			currentImg.hide().parent().append(svg1);
		}
		else if(currentItem.hasClass("var2")){
  			currentImg.hide().parent().append(svg2);
		}
		else if(currentItem.hasClass("var3")){
			currentImg.hide().parent().append(svg3);
		}

		counter1++;




  	})
  }

  if($(".track_item").length){

  	var counter1 = 0;

  	$(".track_item").each(function(){
  		var currentItem = $(this),
  			currentImg  = currentItem.find("img"),
  			imageData   = {
  				width  : currentImg.width(),
  				height : currentImg.height(),
  				url    : currentImg.attr("src")
  			},
			svg1 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 388,0 388,199 0,199 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 388 0 L 388 199 L 0 199 L 0 30 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg2 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,168 357,199 0,199 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0 0 L 388 0 L 388 168 L 357 199 L 0 199 L 0 32 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg4 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 388,0 388,199 357,199 0,199 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0 0 L 388 0 L 388 199 L 357 199 L 0 199 L 0 32 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',	          	
			svg3 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="sm_image_pattern'+counter1+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 388,0 388,168 357,199 0,199 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 388 0 L 388 168 L 357 199 L 0 199 L 0 32 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />' +
				        	'</svg>';

		if(currentItem.hasClass("var1")){
  			currentImg.hide().parent().append(svg1);
		}
		else if(currentItem.hasClass("var2")){
  			currentImg.hide().parent().append(svg2);
		}
		else if(currentItem.hasClass("var3")){
			currentImg.hide().parent().append(svg3);
		}
		else if(currentItem.hasClass("var4")){
			currentImg.hide().parent().append(svg4);
		}

		counter1++;




  	})
  }

  if($(".accordion_svg").length){

  	var counter1 = 0;

  	$(".accordion_svg").each(function(){
  		var currentItem = $(this),
  			currentImg  = currentItem.find("img"),
  			imageData   = {
  				width  : currentImg.width(),
  				height : currentImg.height(),
  				url    : currentImg.attr("src")
  			},
			svg1 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 598,0 598,69 567, 100 0,100 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 598 0 L 598 69 L 0 100 L 0 30 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg2 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="image_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="32,0 598,0 598,122 567, 153 0,153 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 32 0 L 598 0 L 598 122 L 0 153 L 0 30 z" stroke="none" fill-opacity=".2" fill="url(#image_pattern'+counter+')" />',
			svg3 		= 	'<svg width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'<pattern id="sm_image_pattern'+counter1+'" patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				            '<image xlink:href="'+imageData['url']+'" x="1" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
				          	'</pattern>' +
				          	'<polygon points="0,0 389,0 389,117 357,149 0,149 0,32" fill="none" stroke="#fff" stroke-width="1" />' +
				          	'<path d="M 0 0 L 389 0 L 389 117 L 357 149 L 0 149 L 0 32 z" stroke="none" fill-opacity=".8" fill="url(#sm_image_pattern'+counter1+')" />'+
				        	'</svg>';

		if(currentItem.hasClass("var1")){
  			currentImg.hide().parent().append(svg1);
		}
		else if(currentItem.hasClass("var2")){
  			currentImg.hide().parent().append(svg2);
		}
		else if(currentItem.hasClass("var3")){
			currentImg.hide().parent().append(svg3);
		}

		counter1++;




  	})
  }
  // =================tabs start=================

  if(tabsBox.length){
    tabsBox.easyResponsiveTabs();
  }
  // =================tabs end=================

  // =================tags start=================

	if (tagsSelect.length){
		tagsSelect.select2({
			placeholder : $(this).data('placeholder'),
			templateSelection : avatarImages,
			templateResult : avatarImages,
			maximumSelectionLength: 2,
			tags: true,
			adaptDropdownCssClass: function(c) {
		      return c;
		    }
		});
	}

  // =================tags end=================


  // =================modal start=================
  if(modal.length){
      modal.on("click", function(event){
        var modalWindow = $(this).data("modal");

        $(modalWindow).arcticmodal({
        	afterOpen: function() {
		       	var totalH = 0;

		        $('.info_popup_list ul li.top-user').each(function(){

				  var height = $(this).height();

				   totalH += height + 1;

				  console.log(totalH);
				   
				   $('body').find('.info_popup_list_bg').css('height', totalH);
				});

				if(tabsBox.length){
				    tabsBox.easyResponsiveTabs();
			  	}
		   
		    },
        });

        event.preventDefault();
      })
  }
  // =================modal end=================

  // ================= animation for menu ==========

 //  	var $el, leftPos, newWidth;
 //        $mainNav2 = $(".primary_nav");
 //  	$mainNav2.append("<li id='magic-line'></li>");
    
	// var $magicLineTwo = $("#magic-line");

	// $magicLineTwo
	//     .width($(".current").width())
	//     .height($mainNav2.height())
	//     .css("left", $(".current a").position().left)
	//     .data("origLeft", $(".current a").position().left)
	//     .data("origWidth", $magicLineTwo.width())
	//     .data("origColor", $(".current a").attr("rel"));
	            
	// $(".primary_nav a").hover(function() {
	//     $el = $(this);
	//     leftPos = $el.position().left;
	//     newWidth = $el.parent().width();
	//     $magicLineTwo.stop().animate({
	//         left: leftPos,
	//         width: newWidth,
	//         backgroundColor: $el.attr("rel")
	//     })
	// }, function() {
	//     $magicLineTwo.stop().animate({
	//         left: $magicLineTwo.data("origLeft"),
	//         width: $magicLineTwo.data("origWidth"),
	//         backgroundColor: $magicLineTwo.data("origColor")
	//     });    
	// });

	/* Kick IE into gear */
	$(".current a").mouseenter();



	// src картинки вытаскивает в бекраунд начало 
	
		if($(".background_dashbord_img").length){
		    $(".background_dashbord_img").each(function(){
				var current = $(this),
					parent = current.parent(".dashboard_tab_wrapper_container"),
					linkAttr = current.attr("src");

		        parent.css({'background-image' : "url(" + linkAttr + ")"});
		    });
		}

	// src картинки вытаскивает в бекраунд конец 

	if(gerbCarousel.length || gerbCarousel2.length){
		gerbCarousel.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 6,
        	responsive : false,
        	rewindNav : true
		});
		gerbCarousel2.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 5,
        	responsive : false,
        	rewindNav : true
		});
	}
	if(stepCarousel.length){
		stepCarousel.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 5,
        	responsive : false,
        	rewindNav : true
		});
	}
	if(dashboardSlider.length){
		dashboardSlider.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 1,
        	responsive : false,
        	rewindNav : true
		});
	}
	if(professionSlider.length){
		professionSlider.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 1,
        	responsive : false,
        	rewindNav : true,
        	afterInit : attachEvent
		});

		function attachEvent(elem){
			$(".professions_slider_box .owl-prev").mouseenter( function(){
		
				$("body").find(".profession_link_prev").addClass("active");
			});
			$(".professions_slider_box .owl-prev").mouseout( function(){
		
				$("body").find(".profession_link_prev").removeClass("active");
			});

			$(".professions_slider_box .owl-next").mouseenter( function(){
		
				$("body").find(".profession_link_next").addClass("active");
			});
			$(".professions_slider_box .owl-next").mouseout( function(){
		
				$("body").find(".profession_link_next").removeClass("active");
			});
		}
	}
	if(dash2.length){
		dash2.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 1,
        	responsive : false,
        	transitionStyle : "backSlide",
        	mouseDrag : false,
        	rewindNav : true
		});
	}
	if(otherMissionsSlider.length){
		otherMissionsSlider.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : false,
        	items : 4,
        	responsive : false,
        	rewindNav : true
		});
	}
	if(tapeCarousel.length){
		tapeCarousel.owlCarousel({
			navigation : true,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 1,
        	responsive : false,
        	rewindNav : true
		});
	}
	if(materialCarousel.length){
		materialCarousel.owlCarousel({
			navigation : false,
        	navigationText : ["", ""],
        	pagination : true,
        	items : 1,
        	responsive : false,
        	addClassActive : true,
        	rewindNav : true,
        	afterInit: function(){
        		setTimeout(function(){
					var srcImg = $('.material_carousel .owl-item.active .mater_img_hide').attr('src');
			     	$('.place_material > #place_material_id').attr('src', srcImg);     
        		},200);
        	},
    		afterMove : function(){

		     	var leftImgBlock = $('#place_material_id'),
		     		srcImg 		 = $('.owl-item.active').find('> .material_item img.mater_img_hide').attr('src');
		     	
		     	leftImgBlock.attr('src', srcImg);

		    }
		});	
	}

	if(styler.length){
		styler.styler({
			selectSmartPositioning: true
		});
	}
	
	if(msgField.length){

			msgField.on('keydown', function() {
				var current       = $(this),
			        currentVal    = current.val(),
			        targetBox     = $("#ghost_msg"),
			        currentH      = current.outerHeight(),
			        parentBox     = $(".chat_fieild_box"),
			        chatMessages  = $(".chat_messages"),
			        $thisVal      = $(this).val(),
			        thisValLength = $thisVal.length,
			        numberText         = 45;

			    targetBox.text(currentVal);

				if (targetBox.outerHeight() > currentH && !current.hasClass('active')){
					current.addClass('active');
					targetBox.addClass('active');
					chatMessages.addClass('active');
			    }

			    else if(thisValLength < numberText){

					$(this).removeClass("active");
					$("#ghost_msg").removeClass("active");
					$(".chat_messages").removeClass('active');
				}

			});

	}

	/* ------------------------------------------------
	STICKY START
	------------------------------------------------ */

		if (sticky.length){
			sticky.sticky({
		        topspacing: 0,
		        styler: 'is-sticky',
		        animduration: 0,
		        unlockwidth: false,
		        screenlimit: false,
		        sticktype: 'alonemenu'
			});
		};

		if(sticker.length){
			sticker.sticky({ 
				topSpacing: 0, 
				center:true, 
				className:"hey" 
			});
		}

	/* ------------------------------------------------
	STICKY END
	------------------------------------------------ */



	if(scrollContainer.length || scrollMouse.length){

		scrollContainer.each(function(){
			
			var settings = { showArrows: true ,autoReinitialise: true};
			
			scrollContainer.jScrollPane(settings);

		});

		scrollMouse.mousewheel();  

	}


	if($('.accordion_box').length){
      $('.accordion_title').on('click', function(){
        $(this)
          .toggleClass('active')
          .next('.accordion_list')
          .slideToggle()
      });  
    }

    // =================tabs start=================

		if(gallery.length){
			gallery.gallery({
				// autoplay : true;
			});
		}

	// =================tabs end=================

	$("#show_all_characteristics_btn, #hide_all_characteristics_btn").on("click ontouchstart", function(){

		var profileDescription = $('.profile_description'),
			svg = profileDescription.find('svg');

		profileDescription.toggleClass('show_block');

		if(svg.length){
			var height = profileDescription.outerHeight(),
				width = profileDescription.outerWidth(),
				paths = svg.find('path'),
				polygons = svg.find('polygon');

				svg.attr('height', height);
			var coords = generateCoords(width, height);

			polygons.attr('points', coords.join(" "));
			
			// paths.attr('d', 'M ' + coords.shift() + " L " + coords.join(" ") + ' z');
		}

	});

	function generateCoords(width, height){
		var SKEW = 28,
			coords = ['0,0'];

			coords.push(width + ',0'); // top right corner
			coords.push(width + ',' + (height - SKEW)); // bottom right corner 1
			coords.push((width - SKEW) + ',' + height); // bottom right corner 2
			coords.push('0,' + height); // bottom left corner


		return coords;
	}


	if($(".last_new_command_box").length){
		var parent          = $(".last_new_command_box"),
			showBxFigure    = $('.showBlock'),
			hideBxFigure    = $('.hideBlock'),
			buttonShowField = $('.buttons_show_field'),
			prevLink        = $('.link-prev');

		buttonShowField.on("click", function(e){
			parent.addClass('active');
			showBxFigure.hide().addClass('hidden_block');
			hideBxFigure.show().addClass('showing_block');
			event.preventDefault();
		});

		prevLink.on("click", function(e){
			parent.removeClass('active');
			hideBxFigure.hide().removeClass('showing_block');
			showBxFigure.show().removeClass('hidden_block');
			event.preventDefault();
		});

	}

	if(slider3d.length){
			slider3d.waterwheelCarousel({
		      flankingItems: 3,
		      separation:    250
		    });
		    $('#slider3d_prev').on('click', function () {
		      slider3d.prev();
		      return false
		    });
		    $('#slider3d_next').on('click', function () {
		      slider3d.next();
		      return false;
		    });
	}
	
	function imageSrc(){
	    var image           = $('.slider3d_item img'),
			srcImage        = image.attr('src'),
			parent          = image.parent('.slider3d_item');

			parent.each(function(){
				parent.css({
					"background-image": 'url("korpus/../'+ srcImage +'")',
					"background-position": '0 0',
					"background-repeat": 'no-repeat',
					"background-size": "100%"
				});	
			});
  	}
  	imageSrc();

  	if($(".accordoin_list_box").length){

	  	var counter1 = 1;

	  	$('.accordoin_list_item').each(function(){
	  		var currentItem = $(this),
	  			accordionText = $('.accordion_text'),
	  			currentItemWH   = {
	  				width  : accordionText.width(),
	  				height : accordionText.height()
	  			},
	  			svgOption   = {
	  				id           : 'accordoin_list_svg',
	  				stroke       : '#00aeef',
	  				strokeWidth  : '1',
	  				fillOpacity  : '.7',
	  				fill         : '#00506e',
	  				class        : 'accordion_path'
	  			},

				svg1 		= 	'<svg id="'+svgOption['id']+''+counter1+'" width="'+currentItemWH['width']+'" height="'+currentItemWH['height']+'">' +
				                  '<path class="'+svgOption['class']+'" d="M 11 0, L 243 0, 243 40, 0 40, 0 11 Z" stroke="'+svgOption['stroke']+'" stroke-width="'+svgOption['strokeWidth']+'" fill-opacity="'+svgOption['fillOpacity']+'" fill="'+svgOption['fill']+'"></path>' +
				                '</svg>',

				svg2 		= 	'<svg id="'+svgOption['id']+''+counter1+'" width="'+currentItemWH['width']+'" height="'+currentItemWH['height']+'">' +
				                  '<path class="'+svgOption['class']+'" d="M 0 0, L 243 0, 243 40, 0 40, 0 0 Z" stroke="'+svgOption['stroke']+'" stroke-width="'+svgOption['strokeWidth']+'" fill-opacity="'+svgOption['fillOpacity']+'" fill="'+svgOption['fill']+'"></path>' +
				                '</svg>',

				svg3 		= 	'<svg id="'+svgOption['id']+''+counter1+'" width="'+currentItemWH['width']+'" height="'+currentItemWH['height']+'">' +
				                  '<path class="'+svgOption['class']+'" d="M 0 0, L 243 0, 243 29, 231 40, 0 40, Z" stroke="'+svgOption['stroke']+'" stroke-width="'+svgOption['strokeWidth']+'" fill-opacity="'+svgOption['fillOpacity']+'" fill="'+svgOption['fill']+'"></path>' +
				                '</svg>';

			if(currentItem.index() == 0){
	  			currentItem.prepend(svg1);
			}
			else if(currentItem.index() == 1){
				currentItem.prepend(svg2);
			}
			else if(currentItem.index() == 2){
	  			currentItem.prepend(svg3);
			}

			counter1++;

	  	});

	  	function generateAccordionCoords(width, height, index){
			var SKEW = 18,
				coords = [];

				if(index == 'first'){
					coords.push(SKEW + ',0'); // left top coords 1
					coords.push(width + ',0'); // right top  coords 2
					coords.push(width + ',' + height); // right bottom coords 3
					coords.push('0,' + height); // left bottom coords 4
					coords.push('0,' + SKEW); // left height-top coords 5
				}
				else if(index == 'last'){
					coords.push('0,0'); // left top coords 1
					coords.push(width + ',0'); // right top coords 2
					coords.push(width + ',' + (height - SKEW)); // right height-bottom coords 3
					coords.push((width - SKEW) + ',' + height); // right-skew bottom coords 4
					coords.push('0,' + height); // left bottom coords 5
				}
				else{
					coords.push('0,0'); // left top coords 1
					coords.push(width + ',0'); // right top coords 2
					coords.push(width + ',' + height); // right bottom coords 3
					coords.push('0' + ',' + height); // left bottom coords 4
				}
			return coords;
		}

	  	function drawSvg(coords, svg){

			svg.find('path').attr('d', 'M ' + coords.shift() + " L " + coords.join(" ") + ' z');  		

	  	}

	  	$(".accordoin_list_box").on('click', '.accordoin_list_title' , function(event){

	  		var $this = $(this).closest('.accordoin_list_item'),
	  			svg   = $this.find('svg'),
	  			link  = $this.find('a, button'),
	  			hiddenBox = $this.find('.accordoin_list_hide_box');
	  		$this.toggleClass('active');
	  		hiddenBox.slideToggle({ step: function(){

	  			var position = $this.index() == 0 ? 'first' : ($this.index() == $this.closest('.accordoin_list_box').find('.accordoin_list_item').length - 1 ? 'last' : false),
	  				coords = generateAccordionCoords($this.outerWidth(), $this.outerHeight(), position);
	  			
	  			drawSvg(coords, svg);
	  		}});	
	  		event.stopPropagation();
	  		link.on('click', function(event){
				event.stopPropagation();
	  		});
	  		hiddenBox.on('click', function(event){
				event.stopPropagation();
	  		});

	  	});
	  	
	}
  	// module click start
  	$('[class*="module_"]').on('click', function(){
  		if($(this).hasClass('active')){
  			$(this).removeClass('active');
  		}
  		else{
	  		$(this).siblings().removeClass('active');
	  		$(this).addClass('active');
  		}
  	});
  	// module click end


  	if($(".item_slide").length){
	    var counter = 0;

	    setTimeout(function(){

	      $(".item_slide").each(function(){
	        var currentItem = $(this),
	          fillColor   = currentItem.data('fill'),
	          parentSetting   = {
	            width  : currentItem.outerWidth(),
	            height : currentItem.outerHeight()
	          },
	          svg1    =   '<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
	                      '<path d="M 21 0, L 0 18, 0 77, 229 77, 249 61, 249 0, 21 0 z" stroke="rgba(255, 255, 255, 1)" stroke-width="1" fill-opacity=".3" fill="'+fillColor+'" />'; +
	                      '</svg>'


	      if(currentItem){
	        currentItem.prepend(svg1 + '</svg>');
	      }

	      counter++;
	      })

	    },300);
  	}
	
  	var classBox = $('.introductory_information');
	if(classBox.length){
		classBox.on('click', '.show_more_vod-info.first' ,function(){
			classBox
				.closest('.introductory_information')
				.addClass('active')
				.find('.introduct_all_show')
				.hide()
				.next()
				.show();
			setTimeout(function(){
			if($('.introductory_information ').hasClass('active')){
				stripeBoxFunc();
			}
			},100);
		});
		classBox.on('click', '.show_more_vod-info.second' ,function(){
			classBox
				.closest('.introductory_information')
				.removeClass('active')
				.find('.introduct_all_show')
				.show()
				.next()
				.hide();
				$('.introductory_information').find('.stripe_box_val').remove();
		});
		if(!$('.introductory_information ').hasClass('active')){
			setTimeout(function(){
				$('.introductory_information').find('.stripe_box_val').remove();
			},100);
		}
	}

	var tapeBox = $('.tape_events_box');
	if(tapeBox.length){
		var clickBox = $('.tape_events_item'),
			closeBox = $('.icon_close_carousel');
		
		clickBox.on('click', function(){
			$(this).closest(tapeBox).addClass('active');
		});
		closeBox.on('click', function(){
			$(this).closest(tapeBox).removeClass('active');
		});
	}

	if($('.macrogame_menu').length){
		var parentUL	= $('.icon_main_box_item');

		parentUL.on('click', function(){
			var childUL     = $(this).find('.macrogame_menu_list');
			if(childUL.hasClass('active')){
				parentUL.siblings().find(childUL).removeClass('active');
			}

			else if (!childUL.hasClass('active')){
				childUL.closest('.icon_main_box').find('.active').removeClass('active');
				childUL.addClass('active');
			}

			// icon_main_box

		});

	}

	if($(".popup_mission_list").length){
    var counter = 0;

    setTimeout(function(){

      $(".popup_mission_list_item").each(function(){
        var currentItem = $(this),
            parentSetting   = {
              width  : currentItem.outerWidth(),
              height : currentItem.outerHeight()
            },
            currentImgParent = currentItem.find(">.popup_mission_list_div"),
            currentImg  = currentItem.find(".popup_mission_list_div img"),
            imageData   = {
              width  : currentImg.width(),
              height : currentImg.height(),
              url    : currentImg.attr("src")
            },
            svg1    =   '<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
                        '<pattern id="popup_1_'+counter+'"  patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
                        '<image xlink:href="'+imageData['url']+'" x="0" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'"></image>' +
                        '</pattern>' +
                        '<path d="M 20 0, L 0 23, 0 203, 177 203, 200 178, 200 0, 20 0 z" stroke="#999a9a" stroke-width="1" fill-opacity="1" fill="#000" />' +
                        '<path d="M 20 0, L 0 23, 0 203, 177 203, 200 178, 200 0, 20 0 z" stroke="none" fill-opacity="0.2" fill="url(#popup_1_'+counter+')" />' +
                        '</svg>'
                        ,

            svg2    =   '<svg width="'+parentSetting['width']+'" height="'+parentSetting['height']+'">' +
                        '<pattern id="popup_2_'+counter+'"  patternUnits="userSpaceOnUse" width="'+imageData['width']+'" height="'+imageData['height']+'">' +
                        '<image xlink:href="'+imageData['url']+'" x="0" y="0" width="'+imageData['width']+'" height="'+imageData['height']+'"></image>' +
                        '</pattern>' +
                        '<path d="M 20 0, L 0 23, 0 203, 177 203, 200 178, 200 0, 20 0 z" stroke="#59b8c2" stroke-width="1" fill-opacity="1" fill="#042f32" />' +
                        '<path d="M 20 0, L 0 23, 0 203, 177 203, 200 178, 200 0, 20 0 z" stroke="none" fill-opacity=".4" fill="url(#popup_2_'+counter+')" />' +
                        '</svg>';            


      if(currentItem.is('[data-status="passive"]')){
        currentImg.hide();
        currentImgParent.prepend(svg1);
        console.log('pasive')
      }

      else if(currentItem.is('[data-status="active"]')){
        currentImg.hide();
        currentImgParent.prepend(svg2);
        console.log('active')
      }

      counter++;
      })

    },300);
  	}
  	if($('.show_agent_box').length){
  		$('.show_agent_block').hide();
	  	$('.show_click_box').on('click', function(){
	  		$(this).closest('.show_agent_box').toggleClass('active').find('.show_agent_block').slideToggle();
	  	});
  	}


	// suitable_occupation_box
	function showSvgBlock(){

	  	if($(".suitable_occupation_box").length){

		  	var counter = 1;

		  	$('.suitable_occupation_box').each(function(){
		  		var currentItem = $(this),
		  			currentImg  = currentItem.find("> img.suitable_img"),
		  			fillColor   = currentItem.data('fill'),
		  			imageData   = {
		  				width  : currentImg.outerWidth(),
		  				height : currentImg.outerHeight(),
		  				url    : currentImg.attr("src")
		  			},
		  			currentItemWH   = {
		  				width  : currentItem.outerWidth(),
	  					height : currentItem.outerHeight()
		  			},
		  			svgOption   = {
		  				id           : 'suitable_occupation_box',
		  				stroke       : '#ffffff',
		  				class  		 : 'p_abs full_w',
		  				strokeWidth  : '1',
		  				fillOpacity  : '.1',
		  				fillOpacityImg  : '.5'
		  			},

					svg1 		= 	'<svg id="'+svgOption['id']+'"  class="'+svgOption['class']+'" width="'+currentItemWH['width']+'" height="'+currentItemWH['height']+'">' +
					          	'<pattern id="suitable_pattern'+counter+'" patternUnits="userSpaceOnUse" width="'+currentItemWH['width']+'" height="'+currentItemWH['height']+'">' +
					            '<image xlink:href="'+imageData['url']+'" preserveAspectRatio="xMinYMin" x="0" y="0" width="100%" height="100%"></image>' +
					          	'</pattern>' +
					          	'<path d="M 33 0, L 1200 0, 1200 311, 1168 350, 0 350, 0 36 z" stroke="'+svgOption['stroke']+'" stroke-width="'+svgOption['strokeWidth']+'" fill="#'+fillColor+'" fill-opacity="'+svgOption['fillOpacity']+'" />' +
					          	'<path d="M 33 1, L 1200 1, 1199 311, 1168 349, 1 349, 1 36 z" fill-opacity="'+svgOption['fillOpacityImg']+'" fill="url(#suitable_pattern'+counter+')" />';

				if(currentItem){
					currentImg.hide().closest(currentItem).prepend(svg1);
				}

				counter++;

		  	});

		  	function generateAccordionCoords2(width, height){
				var SKEW   = 33,
					SKEW2  = 37,
					coords = [];
					
					coords.push(SKEW + ',0'); 
					coords.push(width + ',0'); 
					coords.push(width + ',' + (height - SKEW2)); 
					coords.push((width - SKEW) + ',' + height); 
					coords.push('0,' + height); 
					coords.push('0,' + SKEW2); 
				
				return coords;
			}

		  	function drawSvg2(coords, svg){

				svg.find('path').attr('d', 'M ' + coords.shift() + " L " + coords.join(" ") + ' z');  		

		  	}

		  	$(".more_info_occupa").on('click', function(event){

		  		var $this = $(this).closest('.suitable_occupation_box'),
		  			svg   = $this.find('svg'),
		  			link  = $this.find('a, button'),
		  			hiddenBox = $this.find('.occup_txt_shows > p.d_none');

		  		$this.toggleClass('active');
		  		hiddenBox.slideToggle({ step: function(){

		  			coords = generateAccordionCoords2($this.outerWidth(), $this.outerHeight());
		  			
		  			drawSvg2(coords, svg);
		  		}});	
		  		event.preventDefault();
		  	});
		  	
		}

	}
	// showSvgBlock();

	if($(".parametres_box_btns").length){

		$(".parametres_box_btns").one('click', '.btn', function(event){
			var $this = $(this);

			if(!$this.hasClass('current') || $this.attr("click") == 1){

				$this.addClass('current');

				$('body').find('.suitable_occupation').slideDown('fast');

				setTimeout(function(){
					showSvgBlock();
				}, 200);

				$this.attr("click", "0");

				event.preventDefault();
			}
			else{
				event.preventDefault();
			}

			event.preventDefault();
		});

	}
	// end of suitable_occupation_box

	// if(tooltip.length){
	// 	tooltip.tooltipster({
	// 		side: 'bottom',
	// 	});
	// }

})

function avatarImages(state) {
  if (!state.id) { return state.text; }

  if(state.element === undefined || state.element.textContent == '') return false;

  var imgTemlate = state.element.value ? '<img src="static/images/' + state.element.value.toLowerCase() + '.jpg" class="select-img-avatar" />' : '';

  
  var $state = $(
    '<span>' + imgTemlate +state.text + '</span>'
  );
  return $state;
};

// $('.top-user').each(function(){

//   var height = $(this).height();

//    $('body').find('.info_popup_list:before').css('height', height);

// });